#!/usr/bin/python2
# -*- coding: utf-8 -*-

import requests
from requests import Request, Session
import os

class Downloader:
    def __init__(self):
        pass

    def GetHumanReadable(self, size,precision=2):
        suffixes=['B','KB','MB','GB','TB']
        suffixIndex = 0
        while size > 1024 and suffixIndex < 4:
            suffixIndex += 1 #increment the index of the suffix
            size = size/1024.0 #apply the division
        return "%.*f%s"%(precision,size,suffixes[suffixIndex])

    def download(self, url, destination):
        if os.path.exists(destination):
            resume_byte_pos = os.path.getsize(destination)
        else:
            resume_byte_pos = 0

        if (resume_byte_pos > 0):
            #print("resuminging download %s " % destination)
            resume_header = {'Range': 'bytes=%s-' % resume_byte_pos}
        else:
            #print("starting download %s " % destination)
            resume_header = {}
        r = requests.get(url, headers=resume_header,stream=True)

        if r.status_code in [200, 206]:
            CHUNK_SIZE = 8192
            bytes_read = 0
            with open(destination, 'ab') as f:
                itrcount = 1
                for chunk in r.iter_content(CHUNK_SIZE):
                    itrcount  = itrcount + 1
                    f.write(chunk)
                    bytes_read += len(chunk)
                    print(self.GetHumanReadable(bytes_read, 2))
        else:
            pass
            #print(r.status_code)
        r.close()

#downloader = Downloader()
# src = 'https://media.tweakers.tv/progressive/account=s7JeEm/item=EL8DIuzSNAmA/file=AB8LJ4TCcMqk/account=s7JeEm/EL8DIuzSNAmA.mp4'
# src = 'http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_1mb.mp4'
# src = 'http://www.maninblack.org/demos/WhereDoAllTheJunkiesComeFrom.mp3'
# src = "http://vod.streamcloud.be/vier_vod_geo/mp4:_definst_/theskyisthelimit/s3/20160412_afl10_10_geboden_willy_naessens.mp4/media_w404976150_0.ts"
# dest = os.getcwd()+'/test.ts' 
# downloader.download(src, dest)