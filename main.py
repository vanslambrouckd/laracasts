#!/usr/bin/python2
# -*- coding: utf-8 -*-

import sys
import time
from robobrowser import RoboBrowser
from urlparse import urlparse
import os
import browsercookie
from time import sleep
import re
from downloader import Downloader
from slugify import slugify
from settings import BASEDIR_DOWNLOADS

class Laracasts:

    def __init__(self):
        #you have to login manually using chrome before running this program
        self.BASE_URL = "https://laracasts.com"

	self.isLoggedIn()
        #if self.isLoggedIn():
            #pass
        #else:
        #    raise Exception('Please login to laracasts using chrome and try again')

        #self.listSeries()
        #print(self.browser.parsed())

    def isLoggedIn(self):
        self.browser = RoboBrowser(parser='html.parser', history=True)
        cj = browsercookie.chrome()
        self.browser.session.cookies = cj

        url = 'https://laracasts.com'
        self.browser.open(url)
        links = self.browser.find_all('a')
        for link in links:
            if link['href'] == '/logout':
                return True

        return False

    def listSeriesList(self, chosen_serie):
        self.clearScreen()
        series_list = self.getSeriesList(self.BASE_URL+chosen_serie['href'])
            
        str_chosen_serie = "Current series: %s" % chosen_serie['title']
        print(str_chosen_serie)
        streep = ''
        for c in str_chosen_serie:
            streep += "-"

        print(streep)

        for index, listitem in enumerate(series_list):
            val = "[%d] %s"
            print(val % (index, listitem['title'])) 

        print('[b] to go back')
        print('[a] download all')

        list_number = raw_input("Please choose item number: ")
        items_to_download = []
        if (list_number == 'b'):
            self.listSeries()
        elif (list_number == 'a'):            
            items_to_download = series_list
        else:
            try:
                items_to_download = series_list[int(list_number)]
            except ValueError:
                self.listSeriesList(chosen_serie)

        for index, chosen_item in enumerate(items_to_download):
            print('Downloading %s' % chosen_item['title'])
            url = self.getVideoUrl(self.BASE_URL+chosen_item['href'])
            dirname_series = self.getSeriesNameFromUrl(chosen_item['href'])
            dirname_series = BASEDIR_DOWNLOADS + '/' + dirname_series

            if not os.path.exists(dirname_series):
                os.makedirs(dirname_series)
            
            filename = str(index).rjust(2, '0')+'-'+slugify(chosen_item['title'])
            dest_file = dirname_series + '/' + filename + '.mp4'
            self.download(url, dest_file)

        self.listSeriesList(chosen_serie)

    def listSeries(self):
        self.clearScreen()
        series = self.getSeries()

        for index, serie in enumerate(series):
            val = "[%d] %s"
            print(val % (index, serie['title']))

        series_number = raw_input("Please choose series number: ")
        try:
            chosen_serie = series[int(series_number)]
        except:
            self.listSeries()

        self.listSeriesList(chosen_serie)

    def clearScreen(self):
        os.system('clear')

    def getSeries(self):
        self.browser.open('https://laracasts.com/series')
        tmp_titles = self.browser.select(".Card__title a")
        #print(self.browser.parsed())

        series = []
        titles = []
        for title in tmp_titles:
            item = {
                'title': title.text.strip('\n').strip().encode('utf-8'),
                'href': title['href'].encode('utf-8')
            }
            series.append(item)
        return series

    def getSeriesList(self, url):
        #print(url)
        #self.browser.follow_link(url)
        #return []
        self.browser.open(url)
        # self.browser.follow_link(url)
        # # print(self.browser.parsed())
        
        tmp_titles = self.browser.select('.Lesson-List__title a')
        serieslist = []
        titles = []
        for title in tmp_titles:
            span = title.find('span')
            if span:
                span.extract()
            item = {
                'title': title.text.strip('\n').strip().encode('utf-8'),
                'href': title['href'].strip('\n').strip().encode('utf-8')
            }
            serieslist.append(item)
        #print(serieslist)
        return serieslist

    def getVideoUrl(self, url_detailpage):
        #https://laracasts.com/series/how-to-use-html5-video-and-videojs/episodes/1
        self.browser.open(url_detailpage)
        #print(self.browser.parsed())
        item_link = self.browser.find('a', {'title': re.compile('Download Video')})
        url = self.BASE_URL + item_link['href']
        return url
       
    def GetHumanReadable(self, size,precision=2):
        suffixes=['B','KB','MB','GB','TB']
        suffixIndex = 0
        while size > 1024 and suffixIndex < 4:
            suffixIndex += 1 #increment the index of the suffix
            size = size/1024.0 #apply the division
        return "%.*f%s"%(precision,size,suffixes[suffixIndex])

    def download(self, url, destination):
        if os.path.exists(destination):
            resume_byte_pos = os.path.getsize(destination)
        else:
            resume_byte_pos = 0

        if (resume_byte_pos > 0):
            #print("resuminging download %s " % destination)
            resume_header = {'Range': 'bytes=%s-' % resume_byte_pos}
        else:
            #print("starting download %s " % destination)
            resume_header = {}
        r = self.browser.session.get(url, headers=resume_header,stream=True)

        filesize = float(r.headers['Content-Length'])
        filesize_formatted = self.GetHumanReadable(filesize)

        starttime_download = time.time()
        if r.status_code in [200, 206]:
            #print('resume from %s ' % resume_byte_pos)
            #print(r.headers)            
            CHUNK_SIZE = 8192
            bytes_read = 0
            with open(destination, 'ab') as f:
                itrcount = 1
                for chunk in r.iter_content(CHUNK_SIZE):
                    itrcount  = itrcount + 1
                    f.write(chunk)
                    bytes_read += len(chunk)
                    total_bytes_downloaded = resume_byte_pos + bytes_read
                    percentage = self.calculatePercentage(total_bytes_downloaded, filesize)
                    total_bytes_downloaded_formatted = self.GetHumanReadable(total_bytes_downloaded, 2)
                    
                    #print('%s / %s / %f percent' % (total_bytes_downloaded_formatted, filesize_formatted, percentage))
                    # sys.stdout.write('%s / %s / %f percent' % (total_bytes_downloaded_formatted, filesize_formatted, percentage) + '\n')
                    # sys.stdout.flush()
                    p = float(percentage/100)
                    self.drawProgressBar(p, starttime_download, 100)
        else:
            pass
            #print(r.status_code)
        r.close()

    def drawProgressBar(self, percent, start, barLen=20):
        sys.stdout.write("\r")
        progress = ""
        for i in range(barLen):
            if i < int(barLen * percent):
                progress += "="
            else:
                progress += " "

        elapsedTime = time.time() - start;
        estimatedRemaining = int(elapsedTime * (1.0/percent) - elapsedTime)

        if (percent == 1.0):
            sys.stdout.write("[ %s ] %.1f%% Elapsed: %im %02is ETA: Done!\n" % 
                (progress, percent * 100, int(elapsedTime)/60, int(elapsedTime)%60))
            sys.stdout.flush()
            return
        else:
            sys.stdout.write("[ %s ] %.1f%% Elapsed: %im %02is ETA: %im%02is " % 
                (progress, percent * 100, int(elapsedTime)/60, int(elapsedTime)%60,
                 estimatedRemaining/60, estimatedRemaining%60))
            sys.stdout.flush()
            return

    def calculatePercentage(self, start, end):
        return (start/end)*100

    def getSeriesNameFromUrl(self, url):
        url_info = urlparse(url)
        arr_path = url_info.path.split('/')
        series_name = arr_path[2]
        return series_name

    def run(self):
        self.listSeries()

laracasts = Laracasts()
laracasts.run()

# def isLoggedIn():
#     browser = RoboBrowser(parser='html.parser', history=True)
#     cj = browsercookie.chrome()
#     browser.session.cookies = cj

#     url = 'https://laracasts.com'
#     browser.open(url)
#     links = browser.find_all('a')
#     for link in links:
#         if link['href'] == '/logout':
#             return True

#     return False

# if not isLoggedIn():
#     raise Exception('Please login to laracasts using chrome and try again')